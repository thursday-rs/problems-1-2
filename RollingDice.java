//************************************************
//File:        PairOfDice - Problem2
//Author:      Scott Royer
//Date:        10/11/2017
//Course:      CPS 100
//
//PROBLEM STATEMENT: 
//Using the Die class defined in this chapter, design and implement 
//a class called PairOfDice, composed of two Die objects. Include methods 
//to set and get the individual die values, a method to roll the dice, and a 
//method that returns the current sum of the two die values. Create a driver 
//class called RollingDice2 to instantiate and use a PairOfDice object. 
//Create a Driver called RollingDice2 to instantiate and use a PairOfDice object.
//
//INPUT:  Requires no user input. Input is by parameters only.
//
//OUTPUT: Returns a Die 1 result a Die 2 result and a Total of Die1+Die2 
//*************************************************

      
  public class RollingDice {

public static void main(String[] args) {

    PairOfDice pairOne = new PairOfDice();
    pairOne.printRoll();
    System.out.println();

    

}

} 