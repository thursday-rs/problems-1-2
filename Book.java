//************************************************
//File:        Book
//Author:      Scott Royer
//Date:        10/11/2017
//Course:      CPS 100
//
//PROBLEM STATEMENT: "Write a class called Book that contains instance data 
// for the title, author, publisher, and copyright date. Define the Book 
// constructor to accept and initialize this data. Include setter and getter 
// methods for all instance data. Include a toString method that returns a formatted, 
// multi-line description of the book. Create a driver class called Bookshelf, 
// whose main method instantiates and updates several Book objects.
//
//INPUT:  Requires no user input. Input is by parameters only.
//
//OUTPUT: Returns a Book Title, Author, Publisher and Copyright Date.
//*************************************************



package assignment3;


public class Book {
	
    //private variables	
    private String _title;
    private String _author;
    private String _publisher;
    private String _copyright;

    //Constructor
    public Book (String title, String author, String publisher, String copyright)
    {
        _title = title;
        _author = author;
        _publisher = publisher;
        _copyright = copyright;
    }
    
    
    //getters
    public String getTitle() {return _title; }
    public String getPublisher() {return _publisher; }
    public String getCopyright() {return _copyright; }
    public String getAuthor() { return _author; }
    
   

    
    
    //setters
    public void setTitle(String Title) { _title = Title; }
    public void setAuthor(String author) { _author = author; }
    public void setPublisher(String publisher) { _publisher = publisher; }
    public void setCopyright(String copyright) { _copyright = copyright; }
    
    public String toString() 
    {
       return "Title: " + _title + "\n" + "Author: " + _author + "\n" + "Publisher: " + _publisher + 
       "Copyright: " + _copyright + "\n\n";}        
    		   
    }


